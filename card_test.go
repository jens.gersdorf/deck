package deck

import (
	"fmt"
	"testing"
)

func ExampleCard_String() {
	fmt.Printf("%s of %ss\n", Ace, Spade)
	fmt.Printf("%s of %ss\n", Jack, Heart)
	fmt.Printf("%s of %ss\n", Two, Diamond)
	fmt.Printf("%s of %ss\n", Three, Club)
	fmt.Printf("%s of %ss\n", Queen, Spade)


	// Output:
	// Ace of Spades
	// Jack of Hearts
	// Two of Diamonds
	// Three of Clubs
	// Queen of Spades
}

func TestNew(t *testing.T) {
	got := New()
	if len(got) != 4*13 {
		t.Errorf("Expected lenght of new Carddeck to be 32, got %d\n", len(got))
	}
}

func TestDefaultSort(t *testing.T) {
	got := New(DefaultSort)
	exp := Card{Suite: Spade, Rank: Ace}
	if got[0] != exp {
		t.Errorf("Expeced %s of %ss, got %s of %ss", exp.Rank, exp.Suite, got[0].Rank, got[0].Suite)
	}
}

func TestSort(t *testing.T) {
	cards := New(Sort(Less))
	exp := Card{Rank: Ace, Suite: Spade}
	if cards[0] != exp {
		t.Error("Expected Ace of Spades as first card. Received:", cards[0])
	}
}

func TestJokers(t *testing.T) {
	cards := New(Jokers(4))
	count := 0
	for _, c := range cards {
		if c.Suite == Joker {
			count++
		}
	}
	if count != 4 {
		t.Error("Expected 4 Jokers, received:", count)
	}
}

func TestShuffle(t *testing.T) {
	cards := New(Deck(1), Shuffle)

	cardset := make(map[Card]struct{})
	for _, c := range cards {
		if _, ok := cardset[c]; ok {
			t.Errorf("Expected all Cards only once - got %s of %ss again\n", c.Rank, c.Suite)
		}
		cardset[c] = struct{}{}
	}
}

func TestFilter(t *testing.T) {
	cards := New(Filter(func(card Card) bool {
		if card.Rank == Two || card.Rank == Three {
			return true
		} else {
			return false
		}

	}))
	for _, c := range cards {
		if c.Rank == Two || c.Rank == Three {
			t.Errorf("Expected no Two or Three, but got %s of %ss\n", c.Rank, c.Suite)
		}
	}
}

func TestDeck(t *testing.T) {
	cards := New(Deck(3))
	if len(cards) != 4*13*3 {
		t.Errorf("Expected %d, got %d cards\n", 3*14*4, len(cards))
	}
}