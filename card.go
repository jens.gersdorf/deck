package deck

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

//go:generate stringer -type=Suite,Rank

type Suite uint8

const (
	Spade Suite = iota
	Diamond
	Club
	Heart
	Joker // this is a special case
)

var suites = [...]Suite{Spade, Diamond, Club, Heart}

type Rank uint8

const (
	_ Rank = iota
	Ace
	Two
	Three
	Four
	Five
	Six
	Seven
	Eight
	Nine
	Ten
	Jack
	Queen
	King
)

const (
	minRank = Ace
	maxRank = King
)

type Card struct {
	Suite Suite
	Rank  Rank
}

type Option func(cards []Card) []Card

func New(opts ...Option) []Card {
	var ret []Card
	for _, suite := range suites {
		for rank := minRank; rank <= maxRank; rank++ {
			ret = append(ret, Card{Suite: suite, Rank: rank})
		}
	}
	for _, opt := range opts {
		ret = opt(ret)
	}
	return ret
}

func absRank(c Card) int {
	return int(c.Suite) * int(maxRank) + int(c.Rank)
}

func Less(cards []Card) func(i, j int) bool {
	return func(i,j int) bool {
		return absRank(cards[i]) < absRank(cards[j])
	}
}

func DefaultSort(cards []Card) []Card {
	sort.Slice(cards, Less(cards))
	return cards
}

func Sort(f func([]Card) func(i,j int) bool) Option {
	return func(cards []Card) []Card {
		sort.Slice(cards, f(cards))
		return cards
	}
}

func Shuffle(cards []Card) []Card {
	ret := make([]Card, len(cards))
	r := rand.New(rand.NewSource(time.Now().Unix()))
	for i,j := range r.Perm(len(cards)) {
		ret[i] = cards[j]
	}
	return ret
}

func Jokers(i int) Option {
	return func(cards []Card) []Card {
		for j := 0; j < i; j++ {
			cards = append(cards, Card{Suite: Joker, Rank: Jack})
		}
		return cards
	}
}

func Filter(cond func(card Card) bool) func(cards []Card) []Card {
	return func(cards []Card)[]Card {
		var ret []Card
		for _, c := range cards {
			if !cond(c) {
				ret = append(ret, c)
			}
		}
		return ret
	}
}

func Deck(n int) func(cards []Card) []Card {
	return func(cards []Card) []Card {
		var ret []Card
		for i := 0; i < n; i++ {
			ret = append(ret, cards...)
		}
		return ret
	}
}

func (c Card) String() string {
	if c.Suite == Joker {
		return c.Suite.String()
	} else {
		return fmt.Sprintf("%s of %ss\n", c.Rank, c.Suite)
	}
}